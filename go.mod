module gitlab.com/sbitio/terraform-provider-hiera5

go 1.15

require (
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/aws/aws-sdk-go v1.25.50 // indirect
	github.com/bmatcuk/doublestar v1.2.4 // indirect
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/google/uuid v1.1.5 // indirect
	github.com/hashicorp/go-getter v1.4.2 // indirect
	github.com/hashicorp/go-uuid v1.0.2 // indirect
	github.com/hashicorp/go-version v1.2.1 // indirect
	github.com/hashicorp/terraform-plugin-sdk v1.6.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/lyraproj/dgo v0.4.4
	github.com/lyraproj/hiera v0.4.6
	github.com/lyraproj/hierasdk v0.4.4
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mitchellh/go-testing-interface v1.0.4 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/posener/complete v1.2.3 // indirect
	github.com/spf13/cast v1.3.1
	github.com/ulikunitz/xz v0.5.9 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	github.com/zclconf/go-cty v1.1.1 // indirect
	github.com/zclconf/go-cty-yaml v1.0.2 // indirect
	go.opencensus.io v0.22.5 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/grpc v1.23.1 // indirect
)
