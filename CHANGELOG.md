## 0.2.8 (Unreleased)

NOTES:
* Improve README.

## 0.2.7 (2021-01-26)

BUG FIXES:

* Leverage upstream bug fixes by updating dependencies (now for real)

## 0.2.6 (2021-01-26)

BUG FIXES:

* Leverage upstream bug fixes by updating dependencies

NOTES:
* Add GitHub mirror + branch + GitHub Actions + gitlab-ci to automate releasing to Terraform's registry. Fix #1

## 0.2.5 (2021-01-25)

FEATURES:
* Add `bool` data source.

NOTES:
* Never released to mantain version compatibility with https://github.com/chriskuchin/terraform-provider-hiera5

## 0.2.4 (2021-01-25)

FEATURES:
* Add `default` option to allow a default value to specified.
* Add docs

NOTES:
* Never released to mantain version compatibility with https://github.com/chriskuchin/terraform-provider-hiera5

## 0.2.3 (2021-01-25)

BUG FIXES:
* Some doc fixes

NOTES:
* Never released to mantain version compatibility with https://github.com/chriskuchin/terraform-provider-hiera5

## 0.2.2 (2020-04-24)

BUG FIXES:

* Leverage upstream bug fixes by updating dependencies

## 0.2.1 (2020-04-22)

BUG FIXES:

* Leverage upstream bug fixes by updating dependencies

## 0.2.0 (2020-02-17)

FEATURES:

* Add `hiera5_json` data source

BUG FIXES:

* Ensure that `lookup` always returns a valid JSON or an error

## 0.1.0 (2020-02-11)

FEATURES:

* Initial implementation
