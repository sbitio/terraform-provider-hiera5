# Terraform Hiera 5 Provider

[![pipeline status](https://gitlab.com/sbitio/terraform-provider-hiera5/badges/master/pipeline.svg)](https://gitlab.com/sbitio/terraform-provider-hiera5/-/commits/master) [![coverage report](https://gitlab.com/sbitio/terraform-provider-hiera5/badges/master/coverage.svg)](https://gitlab.com/sbitio/terraform-provider-hiera5/-/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/sbitio/terraform-provider-hiera5)](https://goreportcard.com/report/sbitio/terraform-provider-hiera5)

[Hiera5 provider on Terraform's Registry](https://registry.terraform.io/providers/sbitio/hiera5/latest)

This provider implements data sources that can be used to perform hierachical data lookups with Hiera.

This is useful for providing configuration values in an environment with a high level of dimensionality or for making values from an existing Puppet deployment available in Terraform.

It's based on [Terraform hiera provider](https://github.com/ribbybibby/terraform-provider-hiera) and [SmilingNavern's fork](https://github.com/SmilingNavern/terraform-provider-gohiera)

## Goals
* Clean implementation based on [Terraform Plugin SDK](https://www.terraform.io/docs/extend/plugin-sdk.html)
* Clean API implementatation based on [Lyra](https://lyraproj.github.io/)'s [Hiera in golang](https://github.com/lyraproj/hiera)
* Painless migration from [Terraform hiera provider](https://github.com/ribbybibby/terraform-provider-hiera), keeping around some naming and data sources

## Requirements
* [Terraform](https://www.terraform.io/downloads.html) 0.12+

## Usage

### Terraform 0.13+

You can install the provider directly from [Terraform's Resgistry](https://registry.terraform.io/providers/sbitio/hiera5/latest):
```
terraform {
  required_providers {
    hiera5 = {
      source = "sbitio/hiera5"
      version = "0.2.7"
    }
  }
}

provider "hiera5" {
  # Configuration options
}
```

### Terraform <0.13

You can get provider's binaries from [releases on GitHub](https://github.com/sbitio/terraform-provider-hiera5/releases) or compile yourself.

### Configuration
To configure the provider:
```hcl
provider "hiera5" {
  # Optional
  config = "~/hiera.yaml"
  # Optional
  scope = {
    environment = "live"
    service     = "api"
    # Complex variables are supported using pdialect
    facts       = "{timezone=>'CET'}"
  }
  # Optional
  merge  = "deep"
}
```

### Data Sources
This provider only implements data sources.

#### Json
To retrieve anything JSON encoded:
```hcl
data "hiera5_json" "aws_tags" {
    key = "aws_tags"
}
```
The following output parameters are returned:
* `id` - matches the key
* `key` - the queried key
* `value` - the returned value, JSON encoded

As Terraform doesn't support nested maps or other more complex data structures this data source makes perfect fit dealing with complex values.

#### Hash (one level)
*Note*: Terraform doesn't support nested maps or other more complex data structures. Any keys containing nested elements won't be returned. Json data source is recomended for complex values
To retrieve a hash:
```hcl
data "hiera5_hash" "aws_tags" {
    key = "aws_tags"
}
```
The following output parameters are returned:
* `id` - matches the key
* `key` - the queried key
* `value` - the hash, represented as a map

#### Array
To retrieve an array:
```hcl
data "hiera5_array" "java_opts" {
    key = "java_opts"
}
```
The following output parameters are returned:
* `id` - matches the key
* `key` - the queried key
* `value` - the array (list)

#### Value
To retrieve any other flat value:
```hcl
data "hiera5" "aws_cloudwatch_enable" {
    key = "aws_cloudwatch_enable"
}
```
The following output parameters are returned:
* `id` - matches the key
* `key` - the queried key
* `value` - the value

All values are returned as strings because Terraform doesn't implement other types like int, float or bool. The values will be implicitly converted into the appropriate type depending on usage.

#### Bool
To retrieve any other flat value:
```hcl
data "hiera5_bool" "enable_spot_instances" {
  key     = "enable_spot_instances"
  default = false
}
```
The following output parameters are returned:
* `id` - matches the key
* `key` - the queried key
* `value` - the value

All values are returned as strings because Terraform doesn't implement other types like int, float or bool. The values will be implicitly converted into the appropriate type depending on usage.


## Example

Take a look at [test-fixtures](./hiera5/test-fixtures)

## Thanks to
* Julien Andrieux for writting [Go tools and GitLab: How to do continuous integration like a boss](https://about.gitlab.com/blog/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/), a really good starting point.
* [chriskuchin](https://github.com/chriskuchin) for writting default value support and other improvements

## Develpment

### Requirements

* [Go](https://golang.org/doc/install) 1.15

### Docker + make

You can use [golang's docker](https://hub.docker.com/_/golang) to test/build/etc. this repository. By example you can run acceptance tests using `docker run --rm -e GO111MODULE=on -e GOFLAGS="-mod=vendor" -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.12 make testacc`

Please refer to the `Makefile` for available commands.

### Notes

[This repository is vendored as recomended on Terraform's docs](https://www.terraform.io/docs/extend/terraform-0.12-compatibility.html#upgrading-to-the-latest-terraform-sdk) you can update vendored modules using `docker run --rm -e GO111MODULE=on -e GOFLAGS="-mod=vendor" -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.15 make update-vendor-patch`
